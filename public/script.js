let fromColor = document.querySelector('#fromColor');
let toColor = document.querySelector('#toColor');
let h3 = document.querySelector('h3');
let body = document.querySelector('body');

function startup() {
    fromColor.value = '#ff0000';
    toColor.value = '#ffff00';
    setGradient();
}

window.addEventListener('load', startup, false);

function setGradient() {
    body.style.background = `linear-gradient(to right, ${fromColor.value},
        ${toColor.value})`;
    h3.textContent = `body {backgroundImage: ${body.style.backgroundImage};}`;
}

fromColor.addEventListener('input', setGradient);

toColor.addEventListener('input', setGradient);

// https://stackoverflow.com/questions/1484506/random-color-generator
function getRandomColor() {
    let letters = '0123456789abcdef';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

let randomBtn = document.querySelector('#randomizeBtn');

randomBtn.addEventListener('click', () => {
    fromColor.value = getRandomColor();
    toColor.value = getRandomColor();
    setGradient();
});
