# Background Generator

[https://vkainth.gitlab.io/background-generator/](https://vkainth.gitlab.io/background-generator/)

## Introduction

A simple CSS background generator that uses linear-gradient to generate beautiful and reusable backgrounds.

## Technologies Used

- HTML and CSS

- JavaScript

- Bootstrap
